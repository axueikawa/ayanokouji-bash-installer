#!/bin/sh

# Legend:
# (A) - Database related operations
# (B) - Aliases related operations
# (C) - Strings related operations

root=$(pwd)

cd ayanokoujibot

# build
export DOTNET_CLI_TELEMETRY_OPTOUT=1
dotnet restore -f --no-cache
dotnet build src/AyanokoujiBot/AyanokoujiBot.csproj -c Release -o output/

# go back
cd "$root"

# move creds from old to new
mv -f ayanokoujibot_old/output/creds.yml ayanokoujibot/output/creds.yml 1>/dev/null 2>&1
# also copy credentials.json for migration purposes
mv -f ayanokoujibot_old/output/credentials.json ayanokoujibot/output/credentials.json 1>/dev/null 2>&1

# on update, strings will be new version, user will have to manually re-add his strings after each update
# as updates may cause big number of strings to become obsolete, changed, etc
# however, old user's strings will be backed up to strings_old

# (C) backup new strings to reverse rewrite
rm -rf ayanokoujibot/output/data/strings_new 1>/dev/null 2>&1
mv -fT ayanokoujibot/output/data/strings ayanokoujibot/output/data/strings_new 1>/dev/null 2>&1
# (C) delete old strings backup
rm -rf ayanokoujibot_old/output/data/strings_old 1>/dev/null 2>&1
rm -rf ayanokoujibot_old/output/data/strings_new 1>/dev/null 2>&1

# (B) backup new aliases to reverse rewrite
mv -f ayanokoujibot/output/data/aliases.yml ayanokoujibot/output/data/aliases_new.yml 1>/dev/null 2>&1

# (A) move old database
mv -f ayanokoujibot_old/output/data/ayanokoujibot.db ayanokoujibot/output/data/ayanokoujibot.db 1>/dev/null 2>&1

# move old data folder contents (and overwrite)
cp -RT ayanokoujibot_old/output/data/ ayanokoujibot/output/data 1>/dev/null 2>&1

# (B) backup old aliases
mv -f ayanokoujibot/output/data/aliases.yml ayanokoujibot/output/data/aliases_old.yml 1>/dev/null 2>&1
# (B) restore new aliases
mv -f ayanokoujibot/output/data/aliases_new.yml ayanokoujibot/output/data/aliases.yml 1>/dev/null 2>&1

# (C) backup old strings
mv -f ayanokoujibot/output/data/strings ayanokoujibot/output/data/strings_old 1>/dev/null 2>&1
# (C) restore new strings
mv -f ayanokoujibot/output/data/strings_new ayanokoujibot/output/data/strings 1>/dev/null 2>&1

rm "$root/rebuild.sh"
exit 0
