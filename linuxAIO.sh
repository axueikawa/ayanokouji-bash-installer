#!/bin/sh
echo ""
echo "Welcome to AyanokoujiBot."
echo "Downloading the latest installer..."
root=$(pwd)

rm "$root/n-menu.sh" 1>/dev/null 2>&1
wget -N https://gitlab.com/axueikawa/ayanokouji-bash-installer/-/raw/main/n-menu.sh

bash n-menu.sh
cd "$root"
rm "$root/n-menu.sh"
exit 0