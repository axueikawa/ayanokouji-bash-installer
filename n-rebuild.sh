#!/bin/sh
root=$(pwd)

# remove old backup
rm -rf ayanokoujibot_old 1>/dev/null 2>&1

# make a new backup
cp -rT ayanokoujibot ayanokoujibot_old 1>/dev/null 2>&1

wget -q -N https://gitlab.com/axueikawa/ayanokouji-bash-installer/-/raw/main/rebuild.sh
bash rebuild.sh

cd "$root"
rm "$root/n-rebuild.sh"
exit 0

