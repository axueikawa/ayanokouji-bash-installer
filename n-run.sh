#!/bin/sh
echo "Running ayanokoujibot"
root=$(pwd)

if hash dotnet 2>/dev/null
then
    echo "Dotnet installed."
else
    echo "Dotnet is not installed. Please install dotnet."
    exit 1
fi

cd "$root/ayanokoujibot/output"
echo "Running ayanokoujibot. Please wait."
dotnet AyanokoujiBot.dll
echo "Done"

cd "$root"
rm "$root/n-run.sh"
exit 0
