#!/bin/sh

root=$(pwd)

# remove old backup
rm -rf ayanokoujibot_old 1>/dev/null 2>&1

# make a new backup
mv -fT ayanokoujibot ayanokoujibot_old 1>/dev/null 2>&1

# clone new version
git clone -b main --recursive --depth 1 https://gitlab.com/axueikawa/ayanokoujibot

wget -q -N https://gitlab.com/axueikawa/nadeko-bash-installer/-/raw/main/rebuild.sh
bash rebuild.sh

cd "$root"
rm "$root/n-download.sh"
exit 0
